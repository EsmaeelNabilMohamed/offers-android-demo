package com.waysgroup.offers.Models.offers;

import java.util.List;

public class OffersResponse {

    /**
     * code : 0
     * data : [{"id":126,"name":"Samo Beauty","description":"Hair color is the pigmentation of hair follicles due to two types of melanin: eumelanin and pheomelanin. Generally, if more eumelanin is present, the color of the hair is darker; if less eumelanin is present, the hair is lighter","start":"2019-07-15","end":"2019-07-25","price":"150","old_price":"200","currency":"EGP","user_id":"19","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":11,"image":"http://3aqrak.com/beauty-apps/public/upload/master768921540637644.jpg","logo":"http://3aqrak.com/beauty-apps/public/upload/master155851563105647.png","open_time":null,"close_time":null,"clinic_id":19,"category_id":1,"day":10,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master768921540637644.jpg","cities":[{"id":12,"offer_id":126,"city_id":12,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Cairo","country_id":2},{"id":7,"offer_id":126,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]},{"id":124,"name":"Ahmed","description":"offer 6 alexoffer 6 alexoffer 6 alexoffer 6 alexoffer 6 alex","start":"2019-07-03","end":"2019-08-03","price":"22","old_price":"77","currency":"EGP","user_id":"3","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":0,"image":"http://3aqrak.com/beauty-apps/public/upload/master839901562592721.jpg","logo":"http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg","open_time":"13:05","close_time":"05:30","clinic_id":3,"category_id":1,"day":31,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master839901562592721.jpg","cities":[{"id":7,"offer_id":124,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]},{"id":123,"name":"Ahmed","description":"offer 5 alexoffer 5 alexoffer 5 alexoffer 5 alex","start":"2019-07-04","end":"2019-07-25","price":"44","old_price":"33","currency":"EGP","user_id":"3","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":0,"image":"http://3aqrak.com/beauty-apps/public/upload/master928861562592657.jpg","logo":"http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg","open_time":"13:05","close_time":"05:30","clinic_id":3,"category_id":1,"day":21,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master928861562592657.jpg","cities":[{"id":7,"offer_id":123,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]},{"id":122,"name":"Ahmed","description":"offer 4 alexoffer 4 alexoffer 4 alexoffer 4 alex","start":"2019-07-05","end":"2019-09-07","price":"444","old_price":"33","currency":"EGP","user_id":"3","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":0,"image":"http://3aqrak.com/beauty-apps/public/upload/master317381562592614.jpg","logo":"http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg","open_time":"13:05","close_time":"05:30","clinic_id":3,"category_id":1,"day":64,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master317381562592614.jpg","cities":[{"id":8,"offer_id":122,"city_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Aswan","country_id":2},{"id":7,"offer_id":122,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]},{"id":121,"name":"Ahmed","description":"offer 3 alexoffer 3 alexoffer 3 alexoffer 3 alexoffer 3 alex","start":"2019-07-03","end":"2019-08-01","price":"44","old_price":"33","currency":"EGP","user_id":"3","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":0,"image":"http://3aqrak.com/beauty-apps/public/upload/master627301562592558.jpg","logo":"http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg","open_time":"13:05","close_time":"05:30","clinic_id":3,"category_id":1,"day":29,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master627301562592558.jpg","cities":[{"id":9,"offer_id":121,"city_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Asyut","country_id":2},{"id":7,"offer_id":121,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]},{"id":120,"name":"Ahmed","description":"offer 2 alexoffer 2 alexoffer 2 alexoffer 2 alex","start":"2019-07-12","end":"2019-08-02","price":"22","old_price":"33","currency":"EGP","user_id":"3","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":0,"image":"http://3aqrak.com/beauty-apps/public/upload/master753631562592517.png","logo":"http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg","open_time":"13:05","close_time":"05:30","clinic_id":3,"category_id":1,"day":21,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master753631562592517.png","cities":[{"id":7,"offer_id":120,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]},{"id":119,"name":"Ahmed","description":"asd sf dsf ds fds ","start":"2019-07-26","end":"2019-08-30","price":"88","old_price":"77","currency":"EGP","user_id":"3","open":"1","remember_token":null,"created_at":null,"updated_at":null,"image_id":0,"image":"http://3aqrak.com/beauty-apps/public/upload/master598161562592442.jpg","logo":"http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg","open_time":"13:05","close_time":"05:30","clinic_id":3,"category_id":1,"day":35,"hour":0,"minuite":0,"is_like":0,"is_cart":0,"img":"http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master598161562592442.jpg","cities":[{"id":12,"offer_id":119,"city_id":12,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Cairo","country_id":2},{"id":9,"offer_id":119,"city_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Asyut","country_id":2},{"id":7,"offer_id":119,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]}]
     * pagination : 1
     * offers_pages : 1
     */

    private String code;
    private int pagination;
    private int offers_pages;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getPagination() {
        return pagination;
    }

    public void setPagination(int pagination) {
        this.pagination = pagination;
    }

    public int getOffers_pages() {
        return offers_pages;
    }

    public void setOffers_pages(int offers_pages) {
        this.offers_pages = offers_pages;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 126
         * name : Samo Beauty
         * description : Hair color is the pigmentation of hair follicles due to two types of melanin: eumelanin and pheomelanin. Generally, if more eumelanin is present, the color of the hair is darker; if less eumelanin is present, the hair is lighter
         * start : 2019-07-15
         * end : 2019-07-25
         * price : 150
         * old_price : 200
         * currency : EGP
         * user_id : 19
         * open : 1
         * remember_token : null
         * created_at : null
         * updated_at : null
         * image_id : 11
         * image : http://3aqrak.com/beauty-apps/public/upload/master768921540637644.jpg
         * logo : http://3aqrak.com/beauty-apps/public/upload/master155851563105647.png
         * open_time : null
         * close_time : null
         * clinic_id : 19
         * category_id : 1
         * day : 10
         * hour : 0
         * minuite : 0
         * is_like : 0
         * is_cart : 0
         * img : http://3aqrak.com/beauty-apps/public/upload/http://3aqrak.com/beauty-apps/public/upload/master768921540637644.jpg
         * cities : [{"id":12,"offer_id":126,"city_id":12,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Cairo","country_id":2},{"id":7,"offer_id":126,"city_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","city":"Alexandria","country_id":2}]
         */

        private int id;
        private String name;
        private String description;
        private String start;
        private String end;
        private String price;
        private String old_price;
        private String currency;
        private String user_id;
        private String open;
        private Object remember_token;
        private Object created_at;
        private Object updated_at;
        private int image_id;
        private String image;
        private String logo;
        private Object open_time;
        private Object close_time;
        private int clinic_id;
        private int category_id;
        private int day;
        private int hour;
        private int minuite;
        private int is_like;
        private int is_cart;
        private String img;
        private List<CitiesBean> cities;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getOld_price() {
            return old_price;
        }

        public void setOld_price(String old_price) {
            this.old_price = old_price;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getOpen() {
            return open;
        }

        public void setOpen(String open) {
            this.open = open;
        }

        public Object getRemember_token() {
            return remember_token;
        }

        public void setRemember_token(Object remember_token) {
            this.remember_token = remember_token;
        }

        public Object getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Object created_at) {
            this.created_at = created_at;
        }

        public Object getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Object updated_at) {
            this.updated_at = updated_at;
        }

        public int getImage_id() {
            return image_id;
        }

        public void setImage_id(int image_id) {
            this.image_id = image_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public Object getOpen_time() {
            return open_time;
        }

        public void setOpen_time(Object open_time) {
            this.open_time = open_time;
        }

        public Object getClose_time() {
            return close_time;
        }

        public void setClose_time(Object close_time) {
            this.close_time = close_time;
        }

        public int getClinic_id() {
            return clinic_id;
        }

        public void setClinic_id(int clinic_id) {
            this.clinic_id = clinic_id;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getHour() {
            return hour;
        }

        public void setHour(int hour) {
            this.hour = hour;
        }

        public int getMinuite() {
            return minuite;
        }

        public void setMinuite(int minuite) {
            this.minuite = minuite;
        }

        public int getIs_like() {
            return is_like;
        }

        public void setIs_like(int is_like) {
            this.is_like = is_like;
        }

        public int getIs_cart() {
            return is_cart;
        }

        public void setIs_cart(int is_cart) {
            this.is_cart = is_cart;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public List<CitiesBean> getCities() {
            return cities;
        }

        public void setCities(List<CitiesBean> cities) {
            this.cities = cities;
        }

        public static class CitiesBean {
            /**
             * id : 12
             * offer_id : 126
             * city_id : 12
             * created_at : -0001-11-30 00:00:00
             * updated_at : -0001-11-30 00:00:00
             * city : Cairo
             * country_id : 2
             */

            private int id;
            private int offer_id;
            private int city_id;
            private String created_at;
            private String updated_at;
            private String city;
            private int country_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getOffer_id() {
                return offer_id;
            }

            public void setOffer_id(int offer_id) {
                this.offer_id = offer_id;
            }

            public int getCity_id() {
                return city_id;
            }

            public void setCity_id(int city_id) {
                this.city_id = city_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public int getCountry_id() {
                return country_id;
            }

            public void setCountry_id(int country_id) {
                this.country_id = country_id;
            }
        }
    }
}
