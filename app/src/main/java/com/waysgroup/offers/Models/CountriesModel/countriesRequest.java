package com.waysgroup.offers.Models.CountriesModel;

public class countriesRequest {
    private String trans;

    public countriesRequest(String trans) {
        this.trans = trans;
    }

    public countriesRequest() {
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }
}
