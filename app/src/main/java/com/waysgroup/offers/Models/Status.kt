package com.waysgroup.Models.offers


import com.google.gson.annotations.SerializedName

data class Status(
    @field:SerializedName("code")
    val code: String? = null,
    @field:SerializedName("msg")
    val msg: String? = null,
    @field:SerializedName("error")
    val error: String? = null
)