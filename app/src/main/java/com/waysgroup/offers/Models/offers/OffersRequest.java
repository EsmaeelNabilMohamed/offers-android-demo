package com.waysgroup.offers.Models.offers;

public class OffersRequest {
    private int user_id;
    private int offset;
    private int category_id;
    private String trans;

    public OffersRequest() {
    }

    public OffersRequest(int user_id, int offset, int category_id, String trans) {
        this.user_id = user_id;
        this.offset = offset;
        this.category_id = category_id;
        this.trans = trans;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }
}
