package com.waysgroup.offers.Models.HomeModel;

public class homeRequest {
    private int clinic_id;
    private int offset = 0;
    private String trans = "en";

    public homeRequest() {
    }

    public homeRequest(int clinic_id, int offset, String trans) {
        this.clinic_id = clinic_id;
        this.offset = offset;
        this.trans = trans;
    }

    public int getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(int clinic_id) {
        this.clinic_id = clinic_id;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }


}
