package com.waysgroup.offers.Models.CountriesModel;

import java.util.List;

public class countriesResponse {


    /**
     * code : 0
     * country : [{"id":1,"name":"الكويت","currency":"KWD","created_at":"2018-05-20 13:46:46","updated_at":"2018-10-09 13:19:05","cities":[{"id":1,"city":"العاصمة","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[{"id":1,"area":"الخالدية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":2,"area":"الدسمة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":3,"area":"الدعية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":4,"area":"الدوحة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":5,"area":"الروضة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":6,"area":"السرة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":7,"area":"الشامية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":8,"area":"الشرق","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":9,"area":"الصالحية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":10,"area":"الصليبخات","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":11,"area":"العديلية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":12,"area":"الفيحاء","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":13,"area":"القادسية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":14,"area":"القبلة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":15,"area":"المرقاب","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":16,"area":"المنصورية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"}]},{"id":2,"city":"حولي","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":3,"city":"الفراونية","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":4,"city":"مبارك الكبير","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":5,"city":"الاحمدي","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":6,"city":"الجهراء","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":2,"name":"مصر","currency":"EGP","created_at":null,"updated_at":"2018-10-09 13:19:18","cities":[{"id":7,"city":"الاسكندرية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":8,"city":"أسوان","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":9,"city":"أسيوط","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":10,"city":"البحيرة","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":11,"city":"بني سويف","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":12,"city":"القاهرة","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":13,"city":"الدقهلية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":14,"city":"دمياط","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":15,"city":"الفيوم","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":16,"city":"الغربية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":17,"city":"الجيزة","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":18,"city":"الاسماعلية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":19,"city":"كفر الشيخ","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":20,"city":"الاقصر","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":21,"city":"مرسي مطرح","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":22,"city":"المنيا","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":23,"city":"المنوفية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":24,"city":"الوادي الجديد","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":25,"city":"شمال سيناء","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":26,"city":"بورسعيد","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":27,"city":"القليوبية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":28,"city":"قنا","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":29,"city":"البحر الاحمر","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":30,"city":"الشرقية","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":31,"city":"سوهاج","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":32,"city":"جنوب سيناء","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":33,"city":"السويس","country_id":2,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":3,"name":"الممكلة العربية السعودية","currency":"SAR","created_at":null,"updated_at":"2018-10-09 13:19:27","cities":[{"id":34,"city":"الباحة","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":35,"city":"الجوف","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":36,"city":"المدينة","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":37,"city":"عسير","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":38,"city":"المنطقة الشرقية","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":39,"city":"حائل","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":40,"city":"جيزان","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":41,"city":"مكة المكرمة","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":42,"city":"نجران","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":43,"city":"الحدود الشمالية","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":44,"city":"القصيم","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":45,"city":"الرياض","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":46,"city":"تبوك","country_id":3,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":4,"name":"الامارات","currency":"AED","created_at":null,"updated_at":null,"cities":[{"id":47,"city":"أبوظبي","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"2018-10-10 09:18:02","states":[]},{"id":48,"city":"دبي","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":49,"city":"الشارقة","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":50,"city":"رأس الخيمة","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":51,"city":"عجمان","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":52,"city":"أم القيوين","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":53,"city":"الفجيرة","country_id":4,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":5,"name":"قطر","currency":"QAR","created_at":null,"updated_at":null,"cities":[{"id":54,"city":"الشمال","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":55,"city":"الخور","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":56,"city":"أم صلال","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":57,"city":"الضعاين\r\n","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":58,"city":"الريان","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":59,"city":"الدوحة","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":60,"city":"الوكرة","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":61,"city":"الشحانية","country_id":5,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":6,"name":"البحرين","currency":"BHD","created_at":"2018-10-10 07:49:30","updated_at":"2018-10-10 07:49:30","cities":[{"id":62,"city":" العاصمة","country_id":6,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":63,"city":" المحرق","country_id":6,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":64,"city":"  الشمالية","country_id":6,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":65,"city":"الجنوبية","country_id":6,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":7,"name":"عمان","currency":"OMR","created_at":"2018-10-10 07:51:27","updated_at":"2018-10-10 07:51:27","cities":[{"id":66,"city":" مسقط","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":67,"city":"  ظـفار ","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":68,"city":"  مسـندم ","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":69,"city":"شمال وجنوب الشـرقية\r\n","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":70,"city":"شمال وجنوب الباطنة","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":71,"city":"  الداخلية ","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":72,"city":"  الظاهـرة","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":73,"city":"  البريمي","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":74,"city":"  الوسـطى ","country_id":7,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":8,"name":"لبنان","currency":"LBP","created_at":"2018-10-10 08:05:27","updated_at":"2018-10-10 08:05:27","cities":[{"id":75,"city":"بيروت","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":76,"city":"جبل لبنان","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":77,"city":"كسروان الفتوح-جبيل\r\n","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":78,"city":"الشمال","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":79,"city":"عكار","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":80,"city":"البقاع","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":81,"city":"بعلبك-الهرمل","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":82,"city":"الجنوب","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":83,"city":"النبطية","country_id":8,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]},{"id":9,"name":"الاردن ","currency":"JOD","created_at":"2018-10-10 08:32:53","updated_at":"2018-10-10 08:32:53","cities":[{"id":84,"city":" العاصمة","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":85,"city":" البلقاء","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":86,"city":" معان","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":87,"city":" المفرق","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":88,"city":"  مادبا","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":89,"city":" عجلون","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":90,"city":" اربد","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":91,"city":" الكرك","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":92,"city":" الزرقاء","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":93,"city":" الطفيلة","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":94,"city":" جرش","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":95,"city":" العقبة","country_id":9,"created_at":"-0001-11-30 00:00:00","updated_at":"2018-10-10 09:14:34","states":[]}]}]
     */

    private String code;
    private List<CountryBean> country;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<CountryBean> getCountry() {
        return country;
    }

    public void setCountry(List<CountryBean> country) {
        this.country = country;
    }

    public static class CountryBean {
        /**
         * id : 1
         * name : الكويت
         * currency : KWD
         * created_at : 2018-05-20 13:46:46
         * updated_at : 2018-10-09 13:19:05
         * cities : [{"id":1,"city":"العاصمة","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[{"id":1,"area":"الخالدية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":2,"area":"الدسمة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":3,"area":"الدعية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":4,"area":"الدوحة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":5,"area":"الروضة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":6,"area":"السرة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":7,"area":"الشامية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":8,"area":"الشرق","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":9,"area":"الصالحية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":10,"area":"الصليبخات","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":11,"area":"العديلية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":12,"area":"الفيحاء","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":13,"area":"القادسية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":14,"area":"القبلة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":15,"area":"المرقاب","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":16,"area":"المنصورية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"}]},{"id":2,"city":"حولي","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":3,"city":"الفراونية","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":4,"city":"مبارك الكبير","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":5,"city":"الاحمدي","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]},{"id":6,"city":"الجهراء","country_id":1,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00","states":[]}]
         */

        private int id;
        private String name;
        private String currency;
        private String created_at;
        private String updated_at;
        private List<CitiesBean> cities;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public List<CitiesBean> getCities() {
            return cities;
        }

        public void setCities(List<CitiesBean> cities) {
            this.cities = cities;
        }

        public static class CitiesBean {
            /**
             * id : 1
             * city : العاصمة
             * country_id : 1
             * created_at : -0001-11-30 00:00:00
             * updated_at : -0001-11-30 00:00:00
             * states : [{"id":1,"area":"الخالدية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":2,"area":"الدسمة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":3,"area":"الدعية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":4,"area":"الدوحة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":5,"area":"الروضة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":6,"area":"السرة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":7,"area":"الشامية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":8,"area":"الشرق","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":9,"area":"الصالحية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":10,"area":"الصليبخات","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":11,"area":"العديلية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":12,"area":"الفيحاء","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":13,"area":"القادسية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":14,"area":"القبلة","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":15,"area":"المرقاب","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"},{"id":16,"area":"المنصورية","city_id":1,"created_at":"2018-07-11 09:24:59","updated_at":"2018-07-11 09:24:59"}]
             */

            private int id;
            private String city;
            private int country_id;
            private String created_at;
            private String updated_at;
            private List<StatesBean> states;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public int getCountry_id() {
                return country_id;
            }

            public void setCountry_id(int country_id) {
                this.country_id = country_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public List<StatesBean> getStates() {
                return states;
            }

            public void setStates(List<StatesBean> states) {
                this.states = states;
            }

            public static class StatesBean {
                /**
                 * id : 1
                 * area : الخالدية
                 * city_id : 1
                 * created_at : 2018-07-11 09:24:59
                 * updated_at : 2018-07-11 09:24:59
                 */

                private int id;
                private String area;
                private int city_id;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getArea() {
                    return area;
                }

                public void setArea(String area) {
                    this.area = area;
                }

                public int getCity_id() {
                    return city_id;
                }

                public void setCity_id(int city_id) {
                    this.city_id = city_id;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }
        }
    }
}
