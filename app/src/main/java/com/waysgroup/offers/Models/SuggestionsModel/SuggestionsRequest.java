package com.waysgroup.offers.Models.SuggestionsModel;

public class SuggestionsRequest {
    private String trans;

    public SuggestionsRequest(String trans) {
        this.trans = trans;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }
}
