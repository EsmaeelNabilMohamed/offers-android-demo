package com.waysgroup.offers.Models;

public class flipModel {
    String title;
    String subtitle;
    private String Url;
    private int Id;

    public flipModel() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public flipModel( String title, String subtitle, String url, int id) {
        this.title = title;
        this.subtitle = subtitle;
        Url = url;
        Id = id;
    }
}


