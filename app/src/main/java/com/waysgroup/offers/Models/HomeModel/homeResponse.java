package com.waysgroup.offers.Models.HomeModel;


import java.util.List;

public class homeResponse{

    /**
     * code : 0
     * clinic : {"clinic_information":[{"id":5,"name":"صالون المرايا","phone":"01010699968","open":"Ù\u2026Ù?ØªÙˆØ­","inst":"","whats":"","snap":"","facebook":"","twitter":"","open_time":"00:00","close_time":"00:00","description":"في مرايا صالون ، مع أكثر من 35 عامًا من الخبرة ، ومساحة تزيد عن 400+ صالون   في 125 مدينة عبر طول البلاد وعرضها","about":"في مرايا صالون ، مع أكثر من 35 عامًا من الخبرة ، ومساحة تزيد عن 400+ صالون   في 125 مدينة عبر طول البلاد وعرضها","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","address":"","google_map":"859, 47.97740520","latitude":"859","longitude":"47.97740520","rank":3}],"slides":[{"id":36,"image":"http://3aqrak.com/beauty-apps/public/upload/master399151562269052.png","link":null},{"id":37,"image":"http://3aqrak.com/beauty-apps/public/upload/master782671562269085.png","link":null},{"id":38,"image":"http://3aqrak.com/beauty-apps/public/upload/master584521560438039.png","link":null},{"id":39,"image":"http://3aqrak.com/beauty-apps/public/upload/master498511560438055.png","link":null}],"reviews":[{"id":6,"rating":3,"title":"ممتاز ","review":"ممتاز ","username":"hamza","user_id":14,"created_at":"2019-07-04 19:08:47"}],"media":[{"id":35,"image":"http://3aqrak.com/beauty-apps/public/upload/master343431562270194.jpg","link":"#"}],"tv":[],"features":[{"id":1,"feature":"واي فاي","icon":"http://3aqrak.com/beauty-apps/public/upload/master324491560005437.png"},{"id":2,"feature":"حمام سباحة ","icon":"http://3aqrak.com/beauty-apps/public/upload/master844711560005446.png"},{"id":3,"feature":"ضيافة","icon":"http://3aqrak.com/beauty-apps/public/upload/master598671560005114.png"},{"id":4,"feature":"مكيف هواء","icon":"http://3aqrak.com/beauty-apps/public/upload/master482411560005516.png"},{"id":6,"feature":"بطاقات أئتمان","icon":"http://3aqrak.com/beauty-apps/public/upload/master502481560005470.png"}],"services":[{"service":"بشرة","id":51,"sub_service":[{"id":33,"sub_service":"العناية بالجسم","time":"45","price":"60","currency":"KWD"}]}],"offers":[{"id":115,"name":"تنظيف البشرة","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master647241540637435.jpg","description":"مجموعة من النظرات تتوافق مع الاتجاهات الحالية وتتماشى مع تفضيلات الجمهور الهندي. يسلط الضوء على عام 2018 \"عرائس الخالدة مرة أخرى\" ، وهي عبارة عن مجموعة تتزاوج مع التقنيات الحديثة والقديمة مع حساسيات العصر الجديد والأساليب الكلاسيكية مع الاتجاهات المعاصرة.","price":"30","currency":"KWD"},{"id":1,"name":"عروض نهاية الاسبوع","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master542861540637571.jpg","description":"عروض نهاية الاسبوع\r\n فلاش معرف الشركة الخاصة بك للتمتع بخصومات خاصة على زياراتك الأسبوعية.","price":"30","currency":"KWD"}],"products":[{"id":8,"name":"ميسلين طلاء الأظافر","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master898831561629644.png","description":"طلاء الأظافر من ميسلين رقم ١٣٢\r\nطلاء أظافر شديد اللمعان بألوان كلاسيكية وعصرية للأظافر الموجودة في دائرة الضوء. تألق اللمعان الشبيه بالمرآة والتصاق جيد حتى للمستخدم الأكثر تلميعًا للأظافر. وافق الجلدية ، ومناسبة للبشرة الحساسة.","price":" 67","currency":"KWD"},{"id":7,"name":"احمر الشفاه ميسلين ","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master198481561629711.png","description":"أحمر شفاه ناعم وكريمي مع راحة ناعمة ومغذية. ينزلق الملمس العصري بسلاسة على الشفاه ويترك الشعور بانعدام الوزن في البشرة الثانية. يضفي اللمعان الدقيق اللامع شكل الشفاه وامتيازها ويعزز جاذبيتك. لا تقاوم الإغراء بفضل الألوان الرائعة مليئة ...","price":" 100","currency":"KWD"},{"id":6,"name":"رموش صناعية فوكس","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master163271561629578.png","description":"تحتوي على زوج واحد من الرموش.\r\nتتداخل هذه الرموش الصناعية بسلاسة مع رموشك الطبيعية.\r\nتتميز هذه الرموش بأنها رموش مؤقتة ويمكن إعادة استخدامها مرة ... ","price":" 133","currency":"KWD"},{"id":5,"name":"كريم أساس انفاليبل","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master866891561629471.png","description":"أحضري كريم الأساس من لوريال باريس واحصلي على بشرة متناسقة في وقت قصير. يمنحكِ كريم الأساس بلون ساند من لوريال باريس مظهرا غير لامع ويقلل من اللمعان لمدة تصل إلى 24 ساعة. اخرجي بثقة بفضل كريم الأساس المقاوم للماء والبخار. علاوة على ذلك، يمتزج كريم الأساس بشكل جميل مع بشرتكِ ولا يتشقق عليها. ينزلق كريم الأساس على البشرة بسهولة وسلاسة ويقوم بامتصاص الزيت الزائد على الفور. لا تقلقي إذا كنتِ ... ","price":" 253","currency":"KWD"},{"id":4,"name":"مايبيلين نيويورك كريم ","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master722031561629320.png","description":"مايبيلين نيويورك كريم أساس دريم مات موس، 04 لايت بورسالين\r\n    يحتوي على عسل النحل\r\n    لأفضل النتائج:\r\n    ضعي اللون على شفاهك بدءًا من منتصف شفتك العليا\r\n    ثم مرريه إلى الأطراف الخارجية وانتقلي بعد ذلك إلى شفتك السُفلى\r\n","price":" 155","currency":"KWD"},{"id":3,"name":"لاء أظافر YOLO - 147 أخضر","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master542001561629117.png","description":"لاء أظافر YOLO - 147 أخضر","price":"27.50","currency":"KWD"},{"id":2,"name":"ميسلين طلاء الأظافر","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master625371561629100.png","description":"طلاء الأظافر من ميسلين رقم ١٣٠\r\nطلاء أظافر شديد اللمعان بألوان كلاسيكية وعصرية للأظافر الموجودة في دائرة الضوء. تألق اللمعان الشبيه بالمرآة والتصاق جيد حتى للمستخدم الأكثر تلميعًا للأظافر. وافق الجلدية ، ومناسبة للبشرة الحساسة.","price":" 67","currency":"KWD"},{"id":1,"name":"احمر الشفاه ميسلين سموث اند كريم","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","image":"http://3aqrak.com/beauty-apps/public/upload/master980371561626759.jpg","description":"أحمر شفاه ناعم وكريمي مع راحة ناعمة ومغذية. ينزلق الملمس العصري بسلاسة على الشفاه ويترك الشعور بانعدام الوزن في البشرة الثانية. يضفي اللمعان الدقيق اللامع شكل الشفاه وامتيازها ويعزز جاذبيتك. لا تقاوم الإغراء بفضل الألوان الرائعة مليئة ...","price":" 103","currency":"KWD"}],"our_team":[{"id":9,"name":"samo","image":"http://3aqrak.com/beauty-apps/public/upload/master321251560435344.png"},{"id":10,"name":"rem","image":"http://3aqrak.com/beauty-apps/public/upload/master575461560435370.png"},{"id":11,"name":"rasha","image":"http://3aqrak.com/beauty-apps/public/upload/master126361560435398.png"},{"id":12,"name":"mai","image":"http://3aqrak.com/beauty-apps/public/upload/master628611560435414.png"}]}
     * pagination : 1
     * offers_pages : 1
     * products_pages : 1
     */

    private int code;
    private ClinicBean clinic;
    private int pagination;
    private int offers_pages;
    private int products_pages;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ClinicBean getClinic() {
        return clinic;
    }

    public void setClinic(ClinicBean clinic) {
        this.clinic = clinic;
    }

    public int getPagination() {
        return pagination;
    }

    public void setPagination(int pagination) {
        this.pagination = pagination;
    }

    public int getOffers_pages() {
        return offers_pages;
    }

    public void setOffers_pages(int offers_pages) {
        this.offers_pages = offers_pages;
    }

    public int getProducts_pages() {
        return products_pages;
    }

    public void setProducts_pages(int products_pages) {
        this.products_pages = products_pages;
    }

    public static class ClinicBean {
        private List<ClinicInformationBean> clinic_information;
        private List<SlidesBean> slides;
        private List<ReviewsBean> reviews;
        private List<MediaBean> media;
        private List<?> tv;
        private List<FeaturesBean> features;
        private List<ServicesBean> services;
        private List<OffersBean> offers;
        private List<ProductsBean> products;
        private List<OurTeamBean> our_team;

        public List<ClinicInformationBean> getClinic_information() {
            return clinic_information;
        }

        public void setClinic_information(List<ClinicInformationBean> clinic_information) {
            this.clinic_information = clinic_information;
        }

        public List<SlidesBean> getSlides() {
            return slides;
        }

        public void setSlides(List<SlidesBean> slides) {
            this.slides = slides;
        }

        public List<ReviewsBean> getReviews() {
            return reviews;
        }

        public void setReviews(List<ReviewsBean> reviews) {
            this.reviews = reviews;
        }

        public List<MediaBean> getMedia() {
            return media;
        }

        public void setMedia(List<MediaBean> media) {
            this.media = media;
        }

        public List<?> getTv() {
            return tv;
        }

        public void setTv(List<?> tv) {
            this.tv = tv;
        }

        public List<FeaturesBean> getFeatures() {
            return features;
        }

        public void setFeatures(List<FeaturesBean> features) {
            this.features = features;
        }

        public List<ServicesBean> getServices() {
            return services;
        }

        public void setServices(List<ServicesBean> services) {
            this.services = services;
        }

        public List<OffersBean> getOffers() {
            return offers;
        }

        public void setOffers(List<OffersBean> offers) {
            this.offers = offers;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public List<OurTeamBean> getOur_team() {
            return our_team;
        }

        public void setOur_team(List<OurTeamBean> our_team) {
            this.our_team = our_team;
        }

        public static class ClinicInformationBean {
            /**
             * id : 5
             * name : صالون المرايا
             * phone : 01010699968
             * open : Ù…Ù?ØªÙˆØ­
             * inst :
             * whats :
             * snap :
             * facebook :
             * twitter :
             * open_time : 00:00
             * close_time : 00:00
             * description : في مرايا صالون ، مع أكثر من 35 عامًا من الخبرة ، ومساحة تزيد عن 400+ صالون   في 125 مدينة عبر طول البلاد وعرضها
             * about : في مرايا صالون ، مع أكثر من 35 عامًا من الخبرة ، ومساحة تزيد عن 400+ صالون   في 125 مدينة عبر طول البلاد وعرضها
             * logo : http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png
             * address :
             * google_map : 859, 47.97740520
             * latitude : 859
             * longitude : 47.97740520
             * rank : 3
             */

            private int id;
            private String name;
            private String phone;
            private String open;
            private String inst;
            private String whats;
            private String snap;
            private String facebook;
            private String twitter;
            private String open_time;
            private String close_time;
            private String description;
            private String about;
            private String logo;
            private String address;
            private String google_map;
            private String latitude;
            private String longitude;
            private int rank;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getOpen() {
                return open;
            }

            public void setOpen(String open) {
                this.open = open;
            }

            public String getInst() {
                return inst;
            }

            public void setInst(String inst) {
                this.inst = inst;
            }

            public String getWhats() {
                return whats;
            }

            public void setWhats(String whats) {
                this.whats = whats;
            }

            public String getSnap() {
                return snap;
            }

            public void setSnap(String snap) {
                this.snap = snap;
            }

            public String getFacebook() {
                return facebook;
            }

            public void setFacebook(String facebook) {
                this.facebook = facebook;
            }

            public String getTwitter() {
                return twitter;
            }

            public void setTwitter(String twitter) {
                this.twitter = twitter;
            }

            public String getOpen_time() {
                return open_time;
            }

            public void setOpen_time(String open_time) {
                this.open_time = open_time;
            }

            public String getClose_time() {
                return close_time;
            }

            public void setClose_time(String close_time) {
                this.close_time = close_time;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getAbout() {
                return about;
            }

            public void setAbout(String about) {
                this.about = about;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGoogle_map() {
                return google_map;
            }

            public void setGoogle_map(String google_map) {
                this.google_map = google_map;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public int getRank() {
                return rank;
            }

            public void setRank(int rank) {
                this.rank = rank;
            }
        }

        public static class SlidesBean {
            /**
             * id : 36
             * image : http://3aqrak.com/beauty-apps/public/upload/master399151562269052.png
             * link : null
             */

            private int id;
            private String image;
            private Object link;
            private int position;

            public int getPosition() {
                return position;
            }

            public void setPosition(int position) {
                this.position = position;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getLink() {
                return link;
            }

            public void setLink(Object link) {
                this.link = link;
            }
        }

        public static class ReviewsBean {
            /**
             * id : 6
             * rating : 3
             * title : ممتاز
             * review : ممتاز
             * username : hamza
             * user_id : 14
             * created_at : 2019-07-04 19:08:47
             */

            private int id;
            private int rating;
            private String title;
            private String review;
            private String username;
            private int user_id;
            private String created_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getRating() {
                return rating;
            }

            public void setRating(int rating) {
                this.rating = rating;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getReview() {
                return review;
            }

            public void setReview(String review) {
                this.review = review;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }

        public static class MediaBean {
            /**
             * id : 35
             * image : http://3aqrak.com/beauty-apps/public/upload/master343431562270194.jpg
             * link : #
             */

            private int id;
            private String image;
            private String link;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }
        }

        public static class FeaturesBean {
            /**
             * id : 1
             * feature : واي فاي
             * icon : http://3aqrak.com/beauty-apps/public/upload/master324491560005437.png
             */

            private int id;
            private String feature;
            private String icon;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getFeature() {
                return feature;
            }

            public void setFeature(String feature) {
                this.feature = feature;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }
        }

        public static class ServicesBean {
            /**
             * service : بشرة
             * id : 51
             * sub_service : [{"id":33,"sub_service":"العناية بالجسم","time":"45","price":"60","currency":"KWD"}]
             */

            private String service;
            private int id;
            private List<SubServiceBean> sub_service;

            public String getService() {
                return service;
            }

            public void setService(String service) {
                this.service = service;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public List<SubServiceBean> getSub_service() {
                return sub_service;
            }

            public void setSub_service(List<SubServiceBean> sub_service) {
                this.sub_service = sub_service;
            }

            public static class SubServiceBean {
                /**
                 * id : 33
                 * sub_service : العناية بالجسم
                 * time : 45
                 * price : 60
                 * currency : KWD
                 */

                private int id;
                private String sub_service;
                private String time;
                private String price;
                private String currency;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getSub_service() {
                    return sub_service;
                }

                public void setSub_service(String sub_service) {
                    this.sub_service = sub_service;
                }

                public String getTime() {
                    return time;
                }

                public void setTime(String time) {
                    this.time = time;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getCurrency() {
                    return currency;
                }

                public void setCurrency(String currency) {
                    this.currency = currency;
                }
            }
        }

        public static class OffersBean {
            /**
             * id : 115
             * name : تنظيف البشرة
             * logo : http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png
             * image : http://3aqrak.com/beauty-apps/public/upload/master647241540637435.jpg
             * description : مجموعة من النظرات تتوافق مع الاتجاهات الحالية وتتماشى مع تفضيلات الجمهور الهندي. يسلط الضوء على عام 2018 "عرائس الخالدة مرة أخرى" ، وهي عبارة عن مجموعة تتزاوج مع التقنيات الحديثة والقديمة مع حساسيات العصر الجديد والأساليب الكلاسيكية مع الاتجاهات المعاصرة.
             * price : 30
             * currency : KWD
             */

            private int id;
            private String name;
            private String logo;
            private String image;
            private String description;
            private String price;
            private String currency;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }
        }

        public static class ProductsBean {
            /**
             * id : 8
             * name : ميسلين طلاء الأظافر
             * logo : http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png
             * image : http://3aqrak.com/beauty-apps/public/upload/master898831561629644.png
             * description : طلاء الأظافر من ميسلين رقم ١٣٢
             طلاء أظافر شديد اللمعان بألوان كلاسيكية وعصرية للأظافر الموجودة في دائرة الضوء. تألق اللمعان الشبيه بالمرآة والتصاق جيد حتى للمستخدم الأكثر تلميعًا للأظافر. وافق الجلدية ، ومناسبة للبشرة الحساسة.
             * price :  67
             * currency : KWD
             */

            private int id;
            private String name;
            private String logo;
            private String image;
            private String description;
            private String price;
            private String currency;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }
        }

        public static class OurTeamBean {
            /**
             * id : 9
             * name : samo
             * image : http://3aqrak.com/beauty-apps/public/upload/master321251560435344.png
             */

            private int id;
            private String name;
            private String image;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}