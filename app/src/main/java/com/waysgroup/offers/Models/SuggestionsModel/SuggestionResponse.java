package com.waysgroup.offers.Models.SuggestionsModel;

import java.util.List;

public class SuggestionResponse {


    /**
     * code : 0
     * category : [{"id":1,"name":"Salon","created_at":"2018-08-01 12:27:58","updated_at":"2018-08-01 12:30:38"},{"id":2,"name":"Center","created_at":"2018-08-01 12:29:16","updated_at":"2018-08-01 12:29:16"},{"id":3,"name":"Spa","created_at":"2018-08-01 12:29:25","updated_at":"2018-08-01 12:29:25"},{"id":4,"name":"Makeup Artist","created_at":"2018-08-01 12:29:43","updated_at":"2018-10-30 08:35:51"}]
     */

    private String code;
    private List<CategoryBean> category;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<CategoryBean> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryBean> category) {
        this.category = category;
    }

    public static class CategoryBean {
        /**
         * id : 1
         * name : Salon
         * created_at : 2018-08-01 12:27:58
         * updated_at : 2018-08-01 12:30:38
         */

        private int id;
        private String name;
        private String created_at;
        private String updated_at;
        private int position;
        private boolean selected;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
