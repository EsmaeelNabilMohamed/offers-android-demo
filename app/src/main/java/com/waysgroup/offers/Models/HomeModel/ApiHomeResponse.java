package com.waysgroup.offers.Models.HomeModel;

import java.util.List;

public class ApiHomeResponse {

    private String code;
    private List<SponsorBean> sponsor;
    private List<LatestOffersBean> latest_offers;
    private List<LatestMembersBean> latest_members;
    private List<SlidesBean> slides;
    private List<TopOffersBean> top_offers;
    private List<InStoreBean> in_store;
    private List<MakeupBean> makeup;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<SponsorBean> getSponsor() {
        return sponsor;
    }

    public void setSponsor(List<SponsorBean> sponsor) {
        this.sponsor = sponsor;
    }

    public List<LatestOffersBean> getLatest_offers() {
        return latest_offers;
    }

    public void setLatest_offers(List<LatestOffersBean> latest_offers) {
        this.latest_offers = latest_offers;
    }

    public List<LatestMembersBean> getLatest_members() {
        return latest_members;
    }

    public void setLatest_members(List<LatestMembersBean> latest_members) {
        this.latest_members = latest_members;
    }

    public List<SlidesBean> getSlides() {
        return slides;
    }

    public void setSlides(List<SlidesBean> slides) {
        this.slides = slides;
    }

    public List<TopOffersBean> getTop_offers() {
        return top_offers;
    }

    public void setTop_offers(List<TopOffersBean> top_offers) {
        this.top_offers = top_offers;
    }

    public List<InStoreBean> getIn_store() {
        return in_store;
    }

    public void setIn_store(List<InStoreBean> in_store) {
        this.in_store = in_store;
    }

    public List<MakeupBean> getMakeup() {
        return makeup;
    }

    public void setMakeup(List<MakeupBean> makeup) {
        this.makeup = makeup;
    }

    public static class SponsorBean {
        /**
         * id : 1
         * link : #
         * image : http://3aqrak.com/beauty-apps/public/upload/master617421558012939.jpg
         */

        private int id;
        private String link;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class LatestOffersBean {
        /**
         * id : 126
         * image : http://3aqrak.com/beauty-apps/public/upload/master768921540637644.jpg
         * logo : http://3aqrak.com/beauty-apps/public/upload/master155851563105647.png
         * name : Samo Beauty
         * description : لون الشعر هو تصبغ بصيلات الشعر بسبب نوعين من الميلانين: eumelanin و pheomelanin. بشكل عام ، إذا كان يوجد مزيد من الأميلانين ، يكون لون الشعر أغمق ؛ إذا كان يوجد أقل من eumelanin ، يكون الشعر أفتح
         * price : 150
         * start : 2019-07-15
         * end : 2019-07-25
         * day : 10
         * hour : 0
         * minuite : 0
         */

        private int id;
        private String image;
        private String logo;
        private String name;
        private String description;
        private String price;
        private String start;
        private String end;
        private int day;
        private int hour;
        private int minuite;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getHour() {
            return hour;
        }

        public void setHour(int hour) {
            this.hour = hour;
        }

        public int getMinuite() {
            return minuite;
        }

        public void setMinuite(int minuite) {
            this.minuite = minuite;
        }
    }

    public static class LatestMembersBean {
        /**
         * id : 15
         * logo : http://3aqrak.com/beauty-apps/public/upload/master648581562767693.png
         * name : الاكاديمية اللبنانية
         * rating : 0
         * open : 1
         */

        private int id;
        private String logo;
        private String name;
        private String rating;
        private String open;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getOpen() {
            return open;
        }

        public void setOpen(String open) {
            this.open = open;
        }
    }

    public static class SlidesBean {
        /**
         * id : 5
         * link : http://malexs.com/tajmeel-web/public
         * image : http://3aqrak.com/beauty-apps/public/upload/master549531530813823.jpg
         */

        private int id;
        private String link;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class TopOffersBean {
        /**
         * id : 126
         * image : http://3aqrak.com/beauty-apps/public/upload/master768921540637644.jpg
         * logo : http://3aqrak.com/beauty-apps/public/upload/master155851563105647.png
         * name : صبغة شعر
         * description : لون الشعر هو تصبغ بصيلات الشعر بسبب نوعين من الميلانين: eumelanin و pheomelanin. بشكل عام ، إذا كان يوجد مزيد من الأميلانين ، يكون لون الشعر أغمق ؛ إذا كان يوجد أقل من eumelanin ، يكون الشعر أفتح
         * price : 150
         * start : 2019-07-15
         * end : 2019-07-25
         * day : 10
         * hour : 0
         * minuite : 0
         */

        private int id;
        private String image;
        private String logo;
        private String name;
        private String description;
        private String price;
        private String start;
        private String end;
        private int day;
        private int hour;
        private int minuite;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getHour() {
            return hour;
        }

        public void setHour(int hour) {
            this.hour = hour;
        }

        public int getMinuite() {
            return minuite;
        }

        public void setMinuite(int minuite) {
            this.minuite = minuite;
        }
    }

    public static class InStoreBean {
        /**
         * id : 9
         * image : http://3aqrak.com/beauty-apps/public/upload/master699491561976098.jpg
         * logo : http://3aqrak.com/beauty-apps/public/upload/master186181560076431.jpg
         * name :  الاولالمنتج الاول
         * description : المنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاولالمنتج الاول
         * price : 2003
         * currency : EGP
         */

        private int id;
        private String image;
        private String logo;
        private String name;
        private String description;
        private String price;
        private String currency;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }

    public static class MakeupBean {
        /**
         * id : 10
         * logo : http://3aqrak.com/beauty-apps/public/upload/master686351561987057.png
         * name : هبة الغمري
         */

        private int id;
        private String logo;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
