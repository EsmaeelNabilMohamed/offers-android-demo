package com.waysgroup.offers.Models.HomeModel;

public class homeRequestTwo {
    private int user_id = 5;
    private int offset = 0;
    private int category_id = 1;
    private String trans = "en";

    public homeRequestTwo() {
    }

    public homeRequestTwo(int user_id, int offset, int category_id, String trans) {
        this.user_id = user_id;
        this.offset = offset;
        this.category_id = category_id;
        this.trans = trans;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }
}
