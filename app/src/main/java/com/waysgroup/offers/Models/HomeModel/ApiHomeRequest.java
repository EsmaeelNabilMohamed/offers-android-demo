package com.waysgroup.offers.Models.HomeModel;

public class ApiHomeRequest {

    private int user_id = 14;
    private String trans = "en";

    public ApiHomeRequest() {
    }

    public ApiHomeRequest(int user_id, String trans) {
        this.user_id = user_id;
        this.trans = trans;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }
}
