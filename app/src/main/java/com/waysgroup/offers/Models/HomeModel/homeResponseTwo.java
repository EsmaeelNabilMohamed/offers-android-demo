package com.waysgroup.offers.Models.HomeModel;


import java.util.List;

public class homeResponseTwo {


    /**
     * code : 0
     * data : [{"id":5,"name":"maraya salon","logo":"http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png","open":"1","avg":"3.0000","is_follow":0}]
     * pagination : 1
     * clinics_pages : 1
     */

    private String code;
    private int pagination;
    private int clinics_pages;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getPagination() {
        return pagination;
    }

    public void setPagination(int pagination) {
        this.pagination = pagination;
    }

    public int getClinics_pages() {
        return clinics_pages;
    }

    public void setClinics_pages(int clinics_pages) {
        this.clinics_pages = clinics_pages;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 5
         * name : maraya salon
         * logo : http://3aqrak.com/beauty-apps/public/upload/master500651562268832.png
         * open : 1
         * avg : 3.0000
         * is_follow : 0
         */

        private int id;
        private String name;
        private String logo;
        private String open;
        private String avg;
        private int is_follow;
        private int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getOpen() {
            return open;
        }

        public void setOpen(String open) {
            this.open = open;
        }

        public String getAvg() {
            return avg;
        }

        public void setAvg(String avg) {
            this.avg = avg;
        }

        public int getIs_follow() {
            return is_follow;
        }

        public void setIs_follow(int is_follow) {
            this.is_follow = is_follow;
        }
    }
}