package com.waysgroup.offers.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.wang.avi.AVLoadingIndicatorView;
import com.waysgroup.offers.Models.HomeModel.ApiHomeResponse;
import com.waysgroup.offers.R;
import com.waysgroup.offers.Utils.JavaUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;
import java.util.List;

public class HomePagerAdapter extends PagerAdapter {

    private ArrayList<ApiHomeResponse.SponsorBean> models;
    private LayoutInflater layoutInflater;
    private Context context;
    ImageView logo_image;
    public HomePagerAdapter(ArrayList<ApiHomeResponse.SponsorBean> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final ApiHomeResponse.SponsorBean model = models.get(position);
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.home_pager_item, container, false);


        ImageView  BackgroundImage = view.findViewById(R.id.pager_item_image);
        AVLoadingIndicatorView loading_indicator = view.findViewById(R.id.avi);
        logo_image = view.findViewById(R.id.imageView);
        Glide.with(context).load(model.getImage()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                loading_indicator.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                loading_indicator.setVisibility(View.GONE);
                return false;
            }
        }).dontAnimate().centerCrop().into(BackgroundImage);

        // TODO: 16/07/19 dummy logo

        Glide.with(context).asBitmap().load("https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500").into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                logo_image.setImageDrawable(JavaUtils.getCircleBitmap(context,resource));
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }
        });


        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }




}
