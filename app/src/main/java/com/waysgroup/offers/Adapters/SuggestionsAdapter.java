package com.waysgroup.offers.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionResponse;
import com.waysgroup.offers.R;

import java.util.ArrayList;

public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.ViewHolder> {


    public static onItemClickListener mItemClickListener;

    public void setOnItemClickListener(onItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface onItemClickListener {
        void onItemClickListener(View view, int position);
    }

    public SuggestionsAdapter(Context mContext, ArrayList<SuggestionResponse.CategoryBean> categories) {
        this.mContext = mContext;
        Categories = categories;
    }




    public Context mContext;
    public SuggestionsAdapter.ViewHolder viewHolder;

    @Override
    public int getItemViewType(int position) {
        return Categories.get(position).getId();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }



    public final ArrayList<SuggestionResponse.CategoryBean> Categories;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.suggestion_item, parent, false);
        viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SuggestionResponse.CategoryBean model = Categories.get(position);
        holder.suggestion_text.setText(model.getName());

        if (model.isSelected()){
            holder.suggestion_pointer.setVisibility(View.VISIBLE);
//            hidePointerFromAllExcept(model.getPosition());
        }else {
            holder.suggestion_pointer.setVisibility(View.INVISIBLE);
        }
    }

    public void hidePointerFromAllExcept(int position) {
        for (int i = 0 ; i < Categories.size();i++ ){
            if (i == position){
                Categories.get(i).setSelected(true);
            }else {
                Categories.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return Categories.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView suggestion_pointer;
        TextView suggestion_text;
        ConstraintLayout root;

         ViewHolder(View itemView) {
            super(itemView);
            this.root = itemView.findViewById(R.id.rootCard_suggestion);
            this.suggestion_pointer = itemView.findViewById(R.id.suggestion_circle);
            this.suggestion_text = itemView.findViewById(R.id.suggestion_text);
            suggestion_pointer.setOnClickListener(this);
            suggestion_text.setOnClickListener(this);
            root.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }


         @Override
         public void onClick(View view) {
             if (mItemClickListener != null) {
                 mItemClickListener.onItemClickListener(view, getAdapterPosition());
             }
         }
     }



}
