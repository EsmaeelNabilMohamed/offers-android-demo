package com.waysgroup.offers.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.Nullable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wang.avi.AVLoadingIndicatorView;
import com.waysgroup.offers.Models.offers.OffersResponse;
import com.waysgroup.offers.R;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;

public class offersFlipAdapter extends BaseAdapter implements OnClickListener {
	Context context;

	public interface Callback{
		public void onPageRequested(int page);
	}


	private LayoutInflater inflater;
	private Callback callback;

	private ArrayList<OffersResponse.DataBean> offers ;

	public offersFlipAdapter(Context context, ArrayList<OffersResponse.DataBean> offers) {
		this.context = context;
		this.offers = offers;
		inflater = LayoutInflater.from(context);
	}

	public void setCallback(Callback callback) {
		this.callback = callback;
	}

	@Override
	public int getCount() {
		return offers.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
//		if (position == -1){
//			return 0;
//		}
		return offers.get(position).getId()+1;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		OffersResponse.DataBean offer = offers.get(position);

		Log.e("ADAPTER",offer.toString());

			convertView = inflater.inflate(R.layout.flipitem, parent, false);
			holder = new ViewHolder(convertView);


			Glide.with(context).load(offer.getImage()).listener(new RequestListener<Drawable>() {
				@Override
				public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
					holder.offer_loading.setVisibility(View.GONE);
					return false;
				}

				@Override
				public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
					holder.offer_loading.setVisibility(View.GONE);
					return false;
				}
			}).dontAnimate().centerCrop().into(holder.background_image);
			Glide.with(context).load(offer.getLogo()).dontAnimate().into(holder.logo);
			holder.user_name_textView.setText(offer.getName());
			holder.offerName_textView.setText(offer.getName());
			holder.remainingTime.setText(CalculateRemainingTime(offer.getDay(),offer.getHour(),offer.getMinuite()));
			holder.price.setText(offer.getPrice());
			holder.currency.setText(offer.getCurrency());

			holder.background_image.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					Toast.makeText(context, offer.getId()+"", Toast.LENGTH_SHORT).show();
				}
			});

		return convertView;
	}

	private String CalculateRemainingTime(int day, int hour, int minuite) {
		return day+"d : "+hour+"h : "+minuite+"m";
	}


	static class ViewHolder{
		LinearLayout root;
		ImageView background_image;
		CircleImageView logo ;
		TextView user_name_textView;
		TextView offerName_textView ;
		TextView remainingTime;
		AVLoadingIndicatorView offer_loading;
		TextView price;
		TextView currency;

		ViewHolder(View view){
			this.offer_loading = view.findViewById(R.id.offer_loading);
			this.background_image = view.findViewById(R.id.image_flip);
			this.logo = view.findViewById(R.id.logo_image);
			this.user_name_textView = view.findViewById(R.id.user_name);
			this.offerName_textView = view.findViewById(R.id.offer_name_textView);
			this.remainingTime = view.findViewById(R.id.timeRemainingTextView);
			this.price = view.findViewById(R.id.price_textView);
			this.currency = view.findViewById(R.id.currency_textView);
		}
	}

	@Override
	public void onClick(View v) {

	}


}
