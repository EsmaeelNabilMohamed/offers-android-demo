package com.waysgroup.offers.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wang.avi.AVLoadingIndicatorView;
import com.waysgroup.offers.Models.HomeModel.homeResponseTwo;
import com.waysgroup.offers.Views.Activities.FlipActivity;
import com.waysgroup.offers.Models.HomeModel.homeResponse;
import com.waysgroup.offers.R;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;

public class HomeSlidesAdapter extends RecyclerView.Adapter<HomeSlidesAdapter.Holder> {


    public void clearDate(){
        slides.clear();
        notifyDataSetChanged();
    }

    public static SuggestionsAdapter.onItemClickListener mItemClickListener;

    public void setOnItemClickListener(SuggestionsAdapter.onItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface onItemClickListener {
        void onItemClickListener(View view, int position);
    }

    private final ArrayList<homeResponseTwo.DataBean> slides;
    private Context mContext;
    private Holder viewHolder;

    public HomeSlidesAdapter(ArrayList<homeResponseTwo.DataBean> slides, Context context)
     {
        this.slides = slides;
        this.mContext = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.slide_item, parent, false);
        viewHolder = new Holder(listItem);
        return viewHolder;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        final homeResponseTwo.DataBean model = slides.get(position);

        if (model.getPosition() == 0 || model.getPosition() == 1) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    Toolbar.LayoutParams.MATCH_PARENT,
                    Toolbar.LayoutParams.WRAP_CONTENT
            );
//            params.setMargins(0, 380, 0, 0);
            params.setMargins(0, 30, 0, 0);

            holder.rootCard.setLayoutParams(params);
        }

        Glide.with(mContext).load(model.getLogo()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.logo);

        Glide.with(mContext).load(model.getLogo())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        return false;
                    }
                }).override(Target.SIZE_ORIGINAL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.error).into(holder.slide_image);

//        holder.slide_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(mContext, FlipActivity.class);
//                intent.putExtra("id", model.getId());
//                mContext.startActivity(intent);
//            }
//        });

        holder.slide_link.setText(model.getName());
    }

    @Override
    public long getItemId(int position) {
        final homeResponseTwo.DataBean model = slides.get(position);
        return model.getId();
    }

    @Override
    public int getItemCount() {
        return slides.size();
    }


    static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView slide_image;
        TextView slide_link;
        CardView rootCard;
        AVLoadingIndicatorView progress;
        CircleImageView logo;
        Holder(View itemView) {
            super(itemView);
            this.logo = itemView.findViewById(R.id.logo_image_home_item);
            this.slide_image = itemView.findViewById(R.id.slide_image);
            this.slide_link = itemView.findViewById(R.id.slide_description);
            this.progress = itemView.findViewById(R.id.loader);
            this.rootCard = itemView.findViewById(R.id.rootCard);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClickListener(view, getAdapterPosition());
            }
        }
    }
}
