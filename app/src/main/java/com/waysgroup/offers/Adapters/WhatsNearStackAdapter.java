package com.waysgroup.offers.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.wang.avi.AVLoadingIndicatorView;
import com.waysgroup.offers.Models.HomeModel.ApiHomeResponse;
import com.waysgroup.offers.R;


import java.util.ArrayList;

public class WhatsNearStackAdapter extends RecyclerView.Adapter<WhatsNearStackAdapter.ViewHolder> {

    private ArrayList<ApiHomeResponse.SponsorBean> models;

    private Context context;

    public ArrayList<ApiHomeResponse.SponsorBean> getModels() {
        return models;
    }

    public void setModels(ArrayList<ApiHomeResponse.SponsorBean> models) {
        this.models = models;
    }

    public WhatsNearStackAdapter(ArrayList<ApiHomeResponse.SponsorBean> models, Context context) {
        this.models = models;
        this.context = context;
        Log.e("TAGA","onecreate view adapter constructor");
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.home_pager_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        Log.e("TAGA","onecreate view adapter");
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ApiHomeResponse.SponsorBean model = models.get(position);
        Log.e("TAGA",model.getImage());
        Glide.with(context)
                .load(model.getImage())
                .centerInside()
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static
    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        AVLoadingIndicatorView loader;

        ViewHolder(View view) {
            super(view);
            this.image = view.findViewById(R.id.pager_item_image);
            this.loader  = view.findViewById(R.id.avi);

        }
    }
}
