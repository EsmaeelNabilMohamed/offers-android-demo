package com.waysgroup.offers.Views.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.waysgroup.offers.Api.ApiHelper;
import com.waysgroup.offers.Api.WebService;
import com.waysgroup.offers.Models.offers.OffersRequest;
import com.waysgroup.offers.Models.offers.OffersResponse;
import com.waysgroup.offers.R;
import com.waysgroup.offers.Adapters.offersFlipAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.emilsjolander.flipview.FlipView;
import se.emilsjolander.flipview.OverFlipMode;

import java.util.ArrayList;

public class FlipActivity extends AppCompatActivity implements FlipView.OnOverFlipListener, offersFlipAdapter.Callback, FlipView.OnFlipListener {

    FlipView flip_view;
    ShimmerFrameLayout dummyConstraint;
    private offersFlipAdapter mAdapter;
    CountDownTimer waitTimer;


    ArrayList<OffersResponse.DataBean> mOfferList;
    private WebService mService;
    int user_id = 14;
    int category_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_flip);

        Intent intent = getIntent();
        category_id = intent.getIntExtra("category_id",0);


        mService = ApiHelper.INSTANCE.getService();
        mOfferList = new ArrayList();
        flip_view = findViewById(R.id.flip_view);
        dummyConstraint = findViewById(R.id.hideme);



        OffersRequest offersRequest = new OffersRequest(
                user_id,
                0,
                category_id,
                "en"
        );

        Log.e("TAG","user id : " + user_id + " cat id : "+ category_id);





        getAllOffers(offersRequest);


        mAdapter.setCallback(this);
        flip_view.setAdapter(mAdapter);
        flip_view.setOnFlipListener(this);
//        flip_view.flipBy(1);
        flip_view.setOverFlipMode(OverFlipMode.GLOW);
        flip_view.setEmptyView(findViewById(R.id.emptyView));
        flip_view.setOnOverFlipListener(this);


    }



    void showDummyData(Boolean show){
        if (show){
            flip_view.setVisibility(View.GONE);
            dummyConstraint.setVisibility(View.VISIBLE);
        }else {
            flip_view.setVisibility(View.VISIBLE);
            dummyConstraint.setVisibility(View.GONE);
        }
    }

    private void getAllOffers(OffersRequest offersRequest) {
        showDummyData(true);
        mService.getAllOffers(offersRequest).enqueue(new Callback<OffersResponse>() {
            @Override
            public void onResponse(Call<OffersResponse> call, Response<OffersResponse> response) {
                //TODO hide loader
                if (response.body() != null) {
                    if (response.body().getCode().equals("0")) {
                        mOfferList.clear();
                        if (response.body().getData().size() == 0) {
                            LinearLayout textView = findViewById(R.id.nothingHere);
                            textView.setVisibility(View.VISIBLE);
                        } else {
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                mOfferList.add(response.body().getData().get(i));
                                Log.e("TAG", response.body().getData().get(i).getLogo());
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                        showDummyData(false);


//                        addDataToList((ArrayList<OffersResponse.DataBean>) response.body().getData());
                    } else {
                        Toast.makeText(FlipActivity.this, "Code= " + response.body().getCode(), Toast.LENGTH_SHORT).show();
                    }
                    ;
                } else {

                    showDummyData(true);

                }
            }

            @Override
            public void onFailure(Call<OffersResponse> call, Throwable t) {
                Toast.makeText(FlipActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                showDummyData(true);
            }
        });
        mAdapter = new offersFlipAdapter(getApplicationContext(), mOfferList);

    }
    @Override
    public void onPageRequested(int page) {
        flip_view.smoothFlipTo(page);

    }
    @Override
    public void onFlippedToPage(FlipView v, int position, long id) {
        Toast.makeText(FlipActivity.this, mOfferList.get(position).getName(), Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onOverFlip(FlipView v, OverFlipMode mode, boolean overFlippingPrevious, float overFlipDistance, float flipDistancePerPage) {
        Log.i("overflip", "overFlipDistance = "+overFlipDistance);
    }

    void startPeakingAnimation() {
        // TODO: 10/07/19 start animation for 2 sec
        // TODO: 10/07/19 hide the layout
        waitTimer = new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long l) {
                flip_view.peakNext(true);
            }
            @Override
            public void onFinish() {
                flip_view.peakNext(true);
            }
        }.start();
    }
}
