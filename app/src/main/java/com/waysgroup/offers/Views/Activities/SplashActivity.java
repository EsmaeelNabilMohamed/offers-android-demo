package com.waysgroup.offers.Views.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.*;
import android.widget.*;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wang.avi.AVLoadingIndicatorView;
import com.waysgroup.offers.Api.ApiHelper;
import com.waysgroup.offers.Api.WebService;
import com.waysgroup.offers.Models.FirstPageAd.adRequest;
import com.waysgroup.offers.Models.FirstPageAd.adResponse;
import com.waysgroup.offers.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    LinearLayout logo_layout;
    TextView progress_text;
    AVLoadingIndicatorView loadingIndicator;
    ImageView logo, ad_image;
    ConstraintLayout adLayout;
    private WebService mService;
    int counter = 5;
    CountDownTimer waitTimer;


    Animation logo_animation_drop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.FullScreen);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initViews();
        mService = ApiHelper.INSTANCE.getService();
        logo_animation_drop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation);

        startLogoAnimation();
    }

    void showAd() {
        final adRequest request = new adRequest(3);
        mService.getAd(request).enqueue(new Callback<adResponse>() {
            @Override
            public void onResponse(Call<adResponse> call, Response<adResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getCode().equals("0")){
                        Glide.with(getApplicationContext()).load(response.body().getData().getImage()).centerInside().listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                loadingIndicator.setVisibility(View.GONE);
                                // TODO: 10/07/19 try this senario
                                gotToHomeAfterAdSeen(1000);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                gotToHomeAfterAdSeen(5000);
                                loadingIndicator.setVisibility(View.GONE);
                                return false;
                            }
                        }).into(ad_image);
                    }else {
                        Toast.makeText(SplashActivity.this, "Server Internal Error => code : 1", Toast.LENGTH_SHORT).show();
                        loadingIndicator.setVisibility(View.GONE);
                        gotToHomeAfterAdSeen(1);
                    }
                }else {
                    Toast.makeText(SplashActivity.this,"ERROR", Toast.LENGTH_SHORT).show();
                    gotToHomeAfterAdSeen(1);

                }

            }

            @Override
            public void onFailure(Call<adResponse> call, Throwable t) {
                loadingIndicator.setVisibility(View.GONE);
            }
        });
    }


    void startLogoAnimation() {
        // TODO: 10/07/19 start animation for 2 sec
        logo.startAnimation(logo_animation_drop);
        // TODO: 10/07/19 hide the layout
        waitTimer = new CountDownTimer(1500,1000) {
            @Override
            public void onTick(long l) { }
            @Override
            public void onFinish() {
                hideLogoLayout();
                showAd();
            }
        }.start();
    }


    void hideLogoLayout() {
        logo_layout.setVisibility(View.GONE);
        adLayout.setVisibility(View.VISIBLE);

    }


    private void initViews() {
        logo = findViewById(R.id.animated_logo);
        ad_image = findViewById(R.id.ad_image);
        progress_text = findViewById(R.id.progress_text);
        logo_layout = findViewById(R.id.logo_layout);
        loadingIndicator = findViewById(R.id.loader);
        adLayout = findViewById(R.id.ad_layout);
    }

    void gotToHomeAfterAdSeen(final int millisInFuture) {
            waitTimer = new CountDownTimer(millisInFuture, 1000) {
                public void onTick(long millisUntilFinished) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            switch (counter) {
                                case 0:
                                    progress_text.setText(" ");
                                    counter--;
                                    progress_text.setVisibility(View.GONE);
                                    break;
                                case 1:
                                    progress_text.setText(String.valueOf(counter));
                                    counter--;
                                    break;
                                case 2:
                                    progress_text.setText(String.valueOf(counter));
                                    counter--;
                                    break;
                                case 3:
                                    progress_text.setText(String.valueOf(counter));
                                    counter--;
                                    break;
                                case 4:
                                    progress_text.setText(String.valueOf(counter));
                                    counter--;
                                    break;
                                case 5:
                                    progress_text.setVisibility(View.VISIBLE);
                                    progress_text.setText(String.valueOf(counter));
                                    counter--;
                                    break;
                            }
                        }
                    });
                }

                public void onFinish() {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }.start();

    }


}
