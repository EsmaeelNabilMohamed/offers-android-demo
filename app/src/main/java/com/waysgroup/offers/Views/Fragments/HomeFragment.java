package com.waysgroup.offers.Views.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.rd.PageIndicatorView;
import com.waysgroup.offers.Adapters.HomePagerAdapter;
import com.waysgroup.offers.Adapters.HomeSlidesAdapter;
import com.waysgroup.offers.Adapters.SuggestionsAdapter;
import com.waysgroup.offers.Api.ApiHelper;
import com.waysgroup.offers.Api.WebService;
import com.waysgroup.offers.Models.HomeModel.ApiHomeRequest;
import com.waysgroup.offers.Models.HomeModel.ApiHomeResponse;
import com.waysgroup.offers.Models.HomeModel.homeRequestTwo;
import com.waysgroup.offers.Models.HomeModel.homeResponseTwo;
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionResponse;
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionsRequest;
import com.waysgroup.offers.R;
import com.waysgroup.offers.Utils.CustomViews.placeHolderRecyclerView;
import com.waysgroup.offers.Views.Activities.FlipActivity;
import com.waysgroup.offers.Views.Activities.HomeActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    ViewPager homePager;
    ArrayList<ApiHomeResponse.SponsorBean> pagerList;
    HomePagerAdapter pagerAdapter;
    ApiHomeRequest pagerHomeRequest;

    PageIndicatorView pageIndicatorView;

    private String TAG = "TAG";
    View rootView;


    LinearLayout searchAndSuggestionsView;

    ArrayList<homeResponseTwo.DataBean> Allslides;
    homeResponseTwo.DataBean slide;


    ArrayList<SuggestionResponse.CategoryBean> AllSuggestions;


    SuggestionResponse.CategoryBean suggestion;

    placeHolderRecyclerView slidesrecyclerView;
    RecyclerView suggestionsRecyclerView;

    homeRequestTwo homeRequesttwo;
    SuggestionsRequest suggestionsRequest;

    private HomeSlidesAdapter slidesAdapter;
    private SuggestionsAdapter suggestionsAdapter;


    BottomNavigationView navigationView;
    private WebService mService;
    ShimmerFrameLayout shimmerLayout;
    SearchView searchView;
    StaggeredGridLayoutManager layoutManager;


    SwipeRefreshLayout refreshLayout;
    private boolean searching = false;
    private int lastQuery = 1;
    private int user_id = 14;

    int scrollDist = 0;
    boolean isVisible = true;
    static final float MINIMUM = 25;

    HomeActivity home;
    Activity activityContext;


    public static HomeFragment newInstance() {

        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        homePager = rootView.findViewById(R.id.homePager);
        pageIndicatorView = rootView.findViewById(R.id.pageIndicatorView);


        shimmerLayout = rootView.findViewById(R.id.hideme_fragmentLanding);
        slidesrecyclerView = rootView.findViewById(R.id.landing_recycler);
        suggestionsRecyclerView = rootView.findViewById(R.id.suggestions_recycler);
        searchAndSuggestionsView = rootView.findViewById(R.id.searchAndSuggestionsView);
        navigationView = getActivity().findViewById(R.id.navigationView);
        refreshLayout = rootView.findViewById(R.id.swiperefresh);
        searchView = rootView.findViewById(R.id.search_view);
        View placeHolder = rootView.findViewById(R.id.place_holder_image);
        mService = ApiHelper.INSTANCE.getService();



        pagerList = new ArrayList<>();
        homePager.setPadding(50, 0, 50, 0);
        pagerHomeRequest = new ApiHomeRequest(14, "en");
        getPagerData(pagerHomeRequest);




        homeRequesttwo = new homeRequestTwo(user_id, 0, lastQuery, "en");

        suggestionsRequest = new SuggestionsRequest("en");

        Allslides = new ArrayList();

        AllSuggestions = new ArrayList();


        LinearLayoutManager horizontalScrollLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        suggestionsRecyclerView.setLayoutManager(horizontalScrollLayoutManager);
        suggestionsRecyclerView.setHasFixedSize(true);
        getSuggestions(suggestionsRequest);
        suggestionsRecyclerView.setAdapter(suggestionsAdapter);

        slidesAdapter = new HomeSlidesAdapter(Allslides,getContext());
        slidesAdapter.setHasStableIds(true);
        layoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        slidesrecyclerView.setLayoutManager(layoutManager);
        slidesrecyclerView.setHasFixedSize(true);
        slidesrecyclerView.showPlaceHolderIfEmpty(placeHolder);
        getSlides(homeRequesttwo);
        slidesrecyclerView.setAdapter(slidesAdapter);
        slidesrecyclerView.setNestedScrollingEnabled(true);
        Log.e(TAG, "OnCreateView Fragment");

        slidesAdapter.setOnItemClickListener(new SuggestionsAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
//                Toast.makeText(getContext(),position+"",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getContext(), FlipActivity.class);
                intent.putExtra("category_id", lastQuery);
                startActivity(intent);
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSlides(homeRequesttwo);
            }
        });

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setIconified(false);
            }
        });

        //Validate the data
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    lastQuery = Integer.parseInt(query);
                    homeRequesttwo = new homeRequestTwo(user_id, 0, Integer.parseInt(query), "ar");
                    getSlides(homeRequesttwo);
                } catch (NumberFormatException e) {
                    getSlides(homeRequesttwo);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        slidesrecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // scroll down
                if (isVisible && scrollDist > MINIMUM) {
                    hide();
                    scrollDist = 0;
                    isVisible = false;
                }
                // scroll up
                else if (!isVisible && scrollDist < -MINIMUM) {
                    show();
                    scrollDist = 0;
                    isVisible = true;
                }

                if ((isVisible && dy > 0) || (!isVisible && dy < 0)) {
                    scrollDist += dy;
                }
            }
        });


        return rootView;
    }

    private void getPagerData(ApiHomeRequest pagerHomeRequest) {
        pagerList.clear();
        mService.getViewPagerData(pagerHomeRequest).enqueue(new Callback<ApiHomeResponse>() {
            @Override
            public void onResponse(Call<ApiHomeResponse> call, Response<ApiHomeResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getCode().equals("0")) {
                        pagerList.clear();
                        // pagerAdapter.clear();
                        if (response.body().getSponsor().size() != 0) {
                            for (int i = 0; i < response.body().getSponsor().size(); i++) {
                                pagerList.add(response.body().getSponsor().get(i));
                                Log.e(TAG, response.body().getSponsor().get(i).getImage());
                            }
                            pageIndicatorView.setVisibility(View.VISIBLE);
                            pagerAdapter = new HomePagerAdapter(pagerList,getContext());
                            pagerAdapter.notifyDataSetChanged();
                            homePager.setAdapter(pagerAdapter);
                            homePager.setOffscreenPageLimit(3);
                            homePager.setCurrentItem(0);
                            showDummyData(false);
                        } else {
                            // no data returned
                            Toast.makeText(getContext(), "Sposor list is empty.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        // api return code = 1
                        Toast.makeText(getContext(), "Server Error Json Code = 1", Toast.LENGTH_SHORT).show();
                        searchView.setIconifiedByDefault(false);
                    }

                } else {
                    // response failed
                    Toast.makeText(getContext(), "Response failed!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiHomeResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Request failed!", Toast.LENGTH_SHORT).show();
            }
        });



    }


    private void show() {
        navigationView.animate().translationY(0f).alpha(1.0f).setInterpolator(new DecelerateInterpolator(2f)).start();
//        searchView.animate().translationY(0).alpha(1.0f).setInterpolator(new DecelerateInterpolator(2)).start();
//        suggestionsRecyclerView.animate().translationY(0).alpha(1.0f).setInterpolator(new DecelerateInterpolator(2)).start();
//        searchAndSuggestionsView.setVisibility(View.VISIBLE);
        searchAndSuggestionsView
                .animate()
                .translationY(0)
                .alpha(1.0f)
                .setInterpolator(new DecelerateInterpolator(2))
                .start();

        //        navigationView.setVisibility(View.VISIBLE);
//        searchView.setVisibility(View.VISIBLE);
//        suggestionsRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hide() {
        navigationView.animate().translationY(1000).alpha(0.0f).setInterpolator(new AccelerateInterpolator(2f)).start();
//        searchView.animate().translationY(-1000).alpha(0.0f).setInterpolator(new AccelerateInterpolator(4)).start();
//        suggestionsRecyclerView.animate().translationY(-1000).alpha(0.0f).setInterpolator(new AccelerateInterpolator(2f)).start();
        searchAndSuggestionsView
                .animate()
                .translationY(-1000)
                .alpha(0.0f)
                .setInterpolator(new DecelerateInterpolator(2))
                .start();
//        searchAndSuggestionsView.setVisibility(View.GONE);
//        navigationView.setVisibility(View.GONE);
//        searchView.setVisibility(View.GONE);
//        suggestionsRecyclerView.setVisibility(View.GONE);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getSuggestions(final SuggestionsRequest suggestionsRequest) {
        AllSuggestions.clear();
        Log.e(TAG, "OnViewCreated getSuggestions");
        mService.getSuggestions(suggestionsRequest).enqueue(new Callback<SuggestionResponse>() {
            @Override
            public void onResponse(Call<SuggestionResponse> call, Response<SuggestionResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("0")) {
                        for (int i = 0; i < response.body().getCategory().size(); i++) {
                            suggestion = response.body().getCategory().get(i);
                            suggestion.setPosition(i);
                            if (i == 0) {
                                suggestion.setSelected(true);
                            }
                            AllSuggestions.add(suggestion);
                            Log.e(TAG, suggestion.getName());
                        }
                        suggestionsAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), "Server Error Json Code = 1 , Suggestions", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "response is not successful", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SuggestionResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        suggestionsAdapter = new SuggestionsAdapter(getContext(), AllSuggestions);
        suggestionsAdapter.setHasStableIds(true);

        suggestionsAdapter.setOnItemClickListener(new SuggestionsAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
//                Toast.makeText(getContext(), position+"", Toast.LENGTH_SHORT).show();
                lastQuery = AllSuggestions.get(position).getId();
                Log.e("TAG", lastQuery + "");
                suggestionsAdapter.hidePointerFromAllExcept(position);
                suggestionsRecyclerView.setItemAnimator(null);
                suggestionsRecyclerView.smoothScrollToPosition(position);
                homeRequesttwo = new homeRequestTwo(user_id, 0, AllSuggestions.get(position).getId(), "en");
                getSlides(homeRequesttwo);
            }
        });
    }

    void showDummyData(Boolean show) {
        if (show) {
            slidesrecyclerView.setVisibility(View.GONE);
            shimmerLayout.setVisibility(View.VISIBLE);
        } else {
            slidesrecyclerView.setVisibility(View.VISIBLE);
            shimmerLayout.setVisibility(View.GONE);
        }
    }

    private void getSlides(final homeRequestTwo homeRequesttwo) {
        refreshLayout.setRefreshing(true);
        Allslides.clear();
        Log.e(TAG, "OnViewCreated GetSlides");
        searching = true;
        mService.populateHome(homeRequesttwo).enqueue(new Callback<homeResponseTwo>() {
            @Override
            public void onResponse(Call<homeResponseTwo> call, Response<homeResponseTwo> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("0")) {
                        Allslides.clear();
                        showDummyData(true);
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            slide = response.body().getData().get(i);
                            slide.setPosition(i);
                            Allslides.add(slide);
                            Log.e(TAG, response.body().getData().get(i).getLogo());
                        }
                        slidesAdapter.notifyDataSetChanged();
                        showDummyData(false);
                        searching = false;
                        refreshLayout.setRefreshing(false);
                        searchView.setIconifiedByDefault(false);
                    } else {
                        Toast.makeText(getContext(), "Server Error Json Code = 1", Toast.LENGTH_SHORT).show();
                        searching = false;
                        refreshLayout.setRefreshing(false);
                        searchView.setIconifiedByDefault(false);

                    }
                } else {
//                    Toast.makeText(getContext(), response.errorBody().contentType().toString(), Toast.LENGTH_SHORT).show();
                    searching = false;
                    refreshLayout.setRefreshing(false);
                    searchView.setIconifiedByDefault(false);

                }
            }

            @Override
            public void onFailure(Call<homeResponseTwo> call, Throwable t) {
                showDummyData(false);
                searching = false;
                refreshLayout.setRefreshing(false);
            }
        });
        slidesAdapter = new HomeSlidesAdapter(Allslides, getContext());
        slidesAdapter.setHasStableIds(true);

        slidesAdapter.setOnItemClickListener(new SuggestionsAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
//                Toast.makeText(getContext(),position+"",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getContext(), FlipActivity.class);
                intent.putExtra("category_id", lastQuery);
                startActivity(intent);
            }
        });



    }

}
