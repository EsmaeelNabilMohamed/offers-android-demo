package com.waysgroup.offers.Views.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.DecelerateInterpolator
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.waysgroup.offers.R
import com.waysgroup.offers.Views.Fragments.TestFragment

class HomeActivity : AppCompatActivity() {

    var testFragment : TestFragment? = null
    lateinit var bottomNavigation : BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        bottomNavigation = findViewById(R.id.navigationView)

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigation.selectedItemId = R.id.navigation_home
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                Toast.makeText(applicationContext,"Home",Toast.LENGTH_SHORT).show()
//                supportFragmentManager.beginTransaction()
//                    .replace(R.id.fragments_container, HomeFragment.newInstance()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_shopping_cart -> {
                Toast.makeText(applicationContext,"Cart",Toast.LENGTH_SHORT).show()
//                    supportFragmentManager.beginTransaction()
//                        .replace(R.id.fragments_container, TestFragment.newInstance()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                Toast.makeText(applicationContext,"Profile",Toast.LENGTH_SHORT).show()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

   public fun hideNavigation(show: Boolean){
        if(show){
            bottomNavigation.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()

        }else{
            bottomNavigation.animate().translationY(-1000f).setInterpolator(DecelerateInterpolator(2f)).start()
        }
    }

}
