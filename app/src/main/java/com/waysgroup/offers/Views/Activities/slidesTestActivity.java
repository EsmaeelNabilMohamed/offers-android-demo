package com.waysgroup.offers.Views.Activities;//package com.waysgroup.offers;
//
//import android.util.Log;
//import android.view.View;
//import android.widget.Toast;
//import androidx.appcompat.app.AppCompatActivity;
//import android.os.Bundle;
//import androidx.recyclerview.widget.RecyclerView;
//import androidx.recyclerview.widget.StaggeredGridLayoutManager;
//import com.facebook.shimmer.ShimmerFrameLayout;
//import com.waysgroup.offers.Adapters.HomeSlidesAdapter;
//import com.waysgroup.offers.Api.ApiHelper;
//import com.waysgroup.offers.Api.WebService;
//import com.waysgroup.offers.Models.HomeModel.homeRequest;
//import com.waysgroup.offers.Models.HomeModel.homeResponse;
//import com.waysgroup.offers.Models.offers.OffersRequest;
//import com.waysgroup.offers.Models.offers.OffersResponse;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//import java.util.ArrayList;
//
//public class slidesTestActivity extends AppCompatActivity {
//
//    private String TAG = "TAG";
//    View rootView;
//    ArrayList<homeResponse.ClinicBean.SlidesBean> Allslides;
//
//    private WebService mService;
//    private HomeSlidesAdapter adapter;
//    homeRequest homeRequest;
//    ShimmerFrameLayout shimmerLayout;
//    RecyclerView recyclerView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_home);
//        Toast.makeText(slidesTestActivity.this, "Hello", Toast.LENGTH_SHORT).show();
//
//        shimmerLayout = findViewById(R.id.hideme_fragmentLanding);
//        recyclerView = findViewById(R.id.landing_recycler);
//
//        mService = ApiHelper.INSTANCE.getService();
//        homeRequest = new homeRequest(5, 0, "ar");
//        Allslides = new ArrayList();
//
//
//        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setHasFixedSize(true);
//
//        getSlides(homeRequest);
//
//        recyclerView.setAdapter(adapter);
//
//
//    }
//
//    private void getSlides(homeRequest homeRequest) {
//        Log.e(TAG,"OnViewCreated GetSlides");
//
//        mService.getSlides(homeRequest).enqueue(new Callback<homeResponse>() {
//            @Override
//            public void onResponse(Call<homeResponse> call, Response<homeResponse> response) {
//                Toast.makeText(slidesTestActivity.this, response.body().getClinic().getOffers().get(0).getImage(), Toast.LENGTH_SHORT).show();
//                for (int i = 0 ; i < response.body().getClinic().getSlides().size() ; i ++){
//                    Allslides.add(response.body().getClinic().getSlides().get(i));
//                }
//                adapter.notifyDataSetChanged();
//
//                Log.e(TAG,response.code()+"");
//            }
//            @Override
//            public void onFailure(Call<homeResponse> call, Throwable t) {
//                showDummyData(false);
//            }
//        });
//        adapter = new HomeSlidesAdapter(Allslides,getApplicationContext());
//    }
//
//    void showDummyData(Boolean show) {
//        if (show) {
//            recyclerView.setVisibility(View.GONE);
//            shimmerLayout.setVisibility(View.VISIBLE);
//        } else {
//            recyclerView.setVisibility(View.VISIBLE);
//            shimmerLayout.setVisibility(View.GONE);
//        }
//    }
//
//}
