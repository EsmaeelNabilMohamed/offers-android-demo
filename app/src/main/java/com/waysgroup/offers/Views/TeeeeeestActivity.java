package com.waysgroup.offers.Views;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.iammert.library.ui.multisearchviewlib.MultiSearchView;
import com.waysgroup.offers.Adapters.HomePagerAdapter;
import com.waysgroup.offers.Adapters.SuggestionsAdapter;
import com.waysgroup.offers.Adapters.WhatsNearStackAdapter;
import com.waysgroup.offers.Api.ApiHelper;
import com.waysgroup.offers.Api.WebService;
import com.waysgroup.offers.Models.HomeModel.ApiHomeRequest;
import com.waysgroup.offers.Models.HomeModel.ApiHomeResponse;
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionResponse;
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionsRequest;
import com.waysgroup.offers.R;
import com.yuyakaido.android.cardstackview.*;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;

public class TeeeeeestActivity extends AppCompatActivity  implements CardStackListener{
private String TAG="TAG";
    private WebService mService;

    CardStackView CardStack;
    WhatsNearStackAdapter StackAdapters;
    ArrayList<ApiHomeResponse.SponsorBean> sponsors;

    ApiHomeRequest pagerHomeRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teeeeeest);
        init();
        CardStack = findViewById(R.id.whatsnear_viewpager_hotels);

        sponsors = new ArrayList<>();
        pagerHomeRequest = new ApiHomeRequest(14, "en");

        getPagerData(pagerHomeRequest);




        CardStackLayoutManager manager = new CardStackLayoutManager(getApplicationContext(),this);
        manager.setStackFrom(StackFrom.Right);
        manager.setVisibleCount(5);
        manager.setTranslationInterval(4.0f);
        manager.setMaxDegree(20.0f);
        manager.setScaleInterval(0.95f);
        manager.setSwipeThreshold(0.4f);
        manager.setDirections(Direction.FREEDOM);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(true);
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        manager.setOverlayInterpolator(new LinearInterpolator()) ;
        CardStack.setLayoutManager(manager);

    }



    private void getPagerData(ApiHomeRequest pagerHomeRequest) {
        mService.getViewPagerData(pagerHomeRequest).enqueue(new Callback<ApiHomeResponse>() {
            @Override
            public void onResponse(Call<ApiHomeResponse> call, Response<ApiHomeResponse> response) {
                if (response.body().getCode().equals("0")) {
                    sponsors.clear();
                    // pagerAdapter.clear();
                    if (response.body().getSponsor().size() != 0) {
                        for (int i = 0; i < response.body().getSponsor().size(); i++) {
                            sponsors.add(response.body().getSponsor().get(i));
                            Log.e("TAG", response.body().getSponsor().get(i).getImage());
                        }
                        Log.e(TAG, "getPagerData: sponsers size  "+ sponsors.size() );
                        StackAdapters = new WhatsNearStackAdapter(sponsors,getApplicationContext());
                        StackAdapters.notifyDataSetChanged();
                        CardStack.setAdapter(StackAdapters);
                        Log.e("TAG","sposor size is : "+sponsors.size());
                    } else {
                        // no data returned
                        Toast.makeText(getApplicationContext(), "Sposor list is empty.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // api return code = 1
                    Toast.makeText(getApplicationContext(), "Server Error Json Code = 1", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiHomeResponse> call, Throwable t) {

            }
        });

    }

    void init() {
        mService = ApiHelper.INSTANCE.getService();
        CardStack = findViewById(R.id.whatsnear_viewpager_hotels);
    }


    @Override
    public void onCardDragging(Direction direction, float ratio) {

    }

    @Override
    public void onCardSwiped(Direction direction) {

    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {

    }

    @Override
    public void onCardDisappeared(View view, int position) {

    }
}
