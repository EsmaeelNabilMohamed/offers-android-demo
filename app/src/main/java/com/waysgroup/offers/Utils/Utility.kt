//package com.waysgroup.offers
//
//import java.util.prefs.Preferences
//
//
//import android.app.Activity
//import android.app.Dialog
//import android.content.Context
//import android.content.Intent
//import android.graphics.Color
//import android.net.ConnectivityManager
//import android.os.Handler
//import android.text.Spannable
//import android.text.SpannableString
//import android.text.TextUtils
//import android.text.style.ForegroundColorSpan
//import android.text.style.RelativeSizeSpan
//import android.util.Log
//import android.view.Gravity
//import android.view.View
//import android.view.Window
//import android.view.WindowManager
//import android.view.inputmethod.InputMethodManager
//import android.widget.Button
//import android.widget.ImageView
//import android.widget.TextView
//import android.widget.Toast
//import androidx.constraintlayout.widget.ConstraintLayout
//import androidx.core.content.ContextCompat
//import androidx.fragment.app.FragmentActivity
//import com.bumptech.glide.Glide
//import com.bumptech.glide.request.RequestOptions
//import com.google.android.material.snackbar.Snackbar
//import com.google.gson.Gson
//
//
//object Utility {
//    private var dialog: Dialog? = null
//
//    fun isValidEmail(email: String): Boolean {
//        Log.e("valid", android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches().toString())
//        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
//    }
//
//    fun showToast(activity: Activity?, msg: String?) {
//        val toast = Toast.makeText(
//            activity, msg, Toast.LENGTH_SHORT
//        )
//        toast.setGravity(Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL, 0, 0)
//        toast.show()
//    }
//
//
//    fun showSnackBar(view: View, msg: String?, activity: Activity?) {
//        val mSnackBar = msg?.let { Snackbar.make(view, it, Snackbar.LENGTH_SHORT) }
//        val textView = mSnackBar?.view?.findViewById(R.id.snackbar_text) as TextView
//        mSnackBar.view.setBackgroundColor(activity?.let { ContextCompat.getColor(it, R.color.white) }
//            ?: 0)
//        activity?.let { textView.setTextColor(ContextCompat.getColor(it, R.color.main)) }
//        mSnackBar.show()
//    }
//
//    fun hideKeyboard(activity: Activity?) {
//        val inputManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        // check if no view has focus:
//        val v = activity.currentFocus ?: return
//        inputManager.hideSoftInputFromWindow(v.windowToken, 0)
//    }
//
//    fun isOnline(context: Context?): Boolean {
//        val cm: ConnectivityManager? = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
//        val netInfo = cm?.activeNetworkInfo
//        return (netInfo != null && netInfo.isConnectedOrConnecting)
//    }
//
//    fun clearAllBackStack(activity: FragmentActivity?) {
//        val fm = activity?.supportFragmentManager
//        if (fm != null) {
//            Log.e("backstack", "not null")
//            for (i in 0 until fm.backStackEntryCount) {
//                fm.popBackStack()
//            }
//        }
//    }
//
//    fun stopMultiClick(view: View?) {
//        view?.isEnabled = false
//        val handler = Handler()
//        handler.postDelayed({ view?.isEnabled = true }, 300)
//    }
//
//    fun setAmountDiffSize(value: String?): SpannableString? {
//        var end: Int? = 0
//        end = if (value?.contains(".") == true) {
//            val valueAmt = value.split(".")
//            valueAmt[0].length
//        } else {
//            value?.length
//        }
//        val span: SpannableString? = SpannableString(value)
//        span?.setSpan(
//            RelativeSizeSpan(1.5f), 0, end ?: 0,
//            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//        )
//        return span
//    }
//
//    fun priceTextCustomFontSize(input: String): SpannableString {
//        val ss1 = SpannableString(input)
//        ss1.setSpan(RelativeSizeSpan(2f), 0, 2, 0)
//        ss1.setSpan(ForegroundColorSpan(Color.WHITE), 0, 5, 0)
//        return ss1
//    }
//
//    fun setImageByGlide(context: Context, imgUrl: String?, imgView: ImageView?) {
//        imgView?.let {
//            Glide.with(context as FragmentActivity)
//                .load(imgUrl)
//                .apply(RequestOptions.circleCropTransform())
//                .into(it)
//        }
//    }
//
//    fun setRectImageByGlide(context: Context, imgUrl: String?, imgView: ImageView?) {
//        imgView?.let {
//            Glide.with(context as FragmentActivity)
//                .load(imgUrl)
//                .into(it)
//        }
//    }
//
//    /**
//     * Save the booking to shared preference
//     *
//     * @param context
//     * @param booking [Booking]
//     */
//
//
//
//    /**
//     * Get booking from the saved shared preference
//     *
//     * @param context
//     * @return
//     */
//
//    /**
//     * show Loader view
//     *
//     * @param activity
//     */
//    @Synchronized
//    fun showLoader(context: Activity?) {
//        hideLoader()
//
//        if (context != null) {
//            dialog = Dialog(context, android.R.style.Theme_Translucent)
//            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
//            dialog?.setContentView(R.layout.loader)
//            dialog?.setCancelable(false)
//            dialog?.show()
//        }
//    }
//
//    /**
//     * hide Loader view
//     */
//    @Synchronized
//    fun hideLoader() {
//        /**
//         * due to handle huge crashes on hide loader
//         * we are unable to identify this crash, so we handle it by try-catch block
//         * IllegalArgumentException: View=com.android.internal.policy.impl.PhoneWindow$DecorView
//         * not attached to window manager
//         */
//        dialog?.dismiss()
//    }
//
//
//}