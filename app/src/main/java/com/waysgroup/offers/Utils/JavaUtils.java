package com.waysgroup.offers.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

public class JavaUtils {

    public static RoundedBitmapDrawable getRoundedBitmap(Context context, int ResourcePathInDrawables){

        Bitmap mbitmap = ((BitmapDrawable) context.getResources().getDrawable(ResourcePathInDrawables)).getBitmap();

        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources() ,mbitmap);
        roundedBitmapDrawable.setCornerRadius( 25f);

        return roundedBitmapDrawable ;

    }

    public static RoundedBitmapDrawable getCircleBitmap(Context context, Bitmap bitmap){
        RoundedBitmapDrawable roundedBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.getResources() ,bitmap);
        roundedBitmapDrawable.setCornerRadius( 2000f);
        return roundedBitmapDrawable ;

    }
}
