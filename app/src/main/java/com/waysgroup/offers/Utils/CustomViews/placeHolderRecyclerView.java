package com.waysgroup.offers.Utils.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class placeHolderRecyclerView extends RecyclerView {

    private List<View> mEmptyViews = Collections.emptyList(); // for creating an empty list



    private AdapterDataObserver mDataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            switchViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            switchViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, @Nullable Object payload) {
            switchViews();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            switchViews();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            switchViews();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            switchViews();
        }
    };

    public placeHolderRecyclerView(@NonNull Context context) {
        super(context);
    }

    public placeHolderRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public placeHolderRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(@Nullable Adapter adapter) {
        super.setAdapter(adapter);
        if (adapter != null){
            adapter.registerAdapterDataObserver(mDataObserver);
        }
        mDataObserver.onChanged();
    }

   public void showPlaceHolderIfEmpty(View ...views){
        mEmptyViews = Arrays.asList(views);
    }

    void switchViews(){
        if (getAdapter() != null && mEmptyViews.isEmpty()){
            if (getAdapter().getItemCount() == 0){

                // have no items to display
                //hide the recyclerView
                hideRecyclerView();
                // Show the placeHolder views
                showPlaceHolder(mEmptyViews);

            }else {

                // have even one item
                showRecyclerView();
                // Hide the placeHolder views
                hidePlaceHolder(mEmptyViews);

            }
        }
    }

    void hideRecyclerView(){
        setVisibility(View.GONE);
    }
    void showRecyclerView(){
        setVisibility(View.VISIBLE);
    }
    void showPlaceHolder(List<View> views){
        for (View view : views){
            view.setVisibility(View.VISIBLE);
        }
    }

    void hidePlaceHolder(List<View> views){
        for (View view : views){
            view.setVisibility(View.GONE);
        }
    }
}
