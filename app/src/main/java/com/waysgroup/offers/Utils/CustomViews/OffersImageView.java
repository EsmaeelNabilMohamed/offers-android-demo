package com.waysgroup.offers.Utils.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import com.makeramen.roundedimageview.RoundedImageView;

public class OffersImageView extends RoundedImageView {
    public OffersImageView(Context context) {
        super(context);
    }

    public OffersImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OffersImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


}
