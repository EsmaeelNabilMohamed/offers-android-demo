package com.waysgroup.offers.Api


import com.waysgroup.offers.Models.CountriesModel.countriesRequest
import com.waysgroup.offers.Models.CountriesModel.countriesResponse
import com.waysgroup.offers.Models.FirstPageAd.adRequest
import com.waysgroup.offers.Models.FirstPageAd.adResponse
import com.waysgroup.offers.Models.HomeModel.*
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionResponse
import com.waysgroup.offers.Models.SuggestionsModel.SuggestionsRequest
import com.waysgroup.offers.Models.offers.OffersRequest
import com.waysgroup.offers.Models.offers.OffersResponse
import retrofit2.Call
import retrofit2.http.*


/**
 * @author Davinder Goel
 */

interface WebService {

    @POST("single_category_offers")
    fun getAllOffers(@Body RequestModel: OffersRequest): Call<OffersResponse>

    @POST("home")
    fun getCountries(@Body RequestModel: countriesRequest) : Call<countriesResponse>

    @POST("home")
    fun getViewPagerData(@Body RequestModel: ApiHomeRequest) : Call<ApiHomeResponse>

    @POST("single_clinic")
    fun getSlides(@Body RequestModel: homeRequest) : Call<homeResponse>

    @POST("first_ad")
    fun getAd(@Body RequestModel: adRequest) : Call<adResponse>

    @POST("categories")
    fun getSuggestions(@Body RequestModel: SuggestionsRequest) : Call<SuggestionResponse>


    @POST("single_category_clinics")
    fun populateHome(@Body RequestModel: homeRequestTwo) : Call<homeResponseTwo>

}
