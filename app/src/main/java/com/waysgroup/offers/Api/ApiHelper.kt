package com.waysgroup.offers.Api

import android.os.Build
import android.util.Log
import com.waysgroup.Models.offers.Status
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.TlsVersion
import okhttp3.internal.platform.Platform
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext

/**
 * @author Openkey Inc.
 */
object ApiHelper {
    private const val TIMEOUT = 30L
    private val mRetrofit: Retrofit
    private val mService: WebService
    private val BASE_URL :String = "http://3aqrak.com/beauty-apps/public/api/"
//    private val BASE_URL :String = "https://beauty-apps.com/api/"

    // Creating Retrofit Object
    init {
        mRetrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getClient())
            .build()
        mService = mRetrofit.create(WebService::class.java)
    }

    // Creating OkHttpclient Object
    private fun getClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)

        okHttpClient.addInterceptor{ chain ->
            val request = chain.request().newBuilder()
            request.addHeader("Accept", "application/json")
            request.addHeader("cache-control", "no-cache")
            chain.proceed(request.build())
        }

        return enableTls12OnPreLollipop(okHttpClient).build()
    }

    fun getService(): WebService {
        return mService
    }


    private fun enableTls12OnPreLollipop(client: OkHttpClient.Builder): OkHttpClient.Builder {
        if (Build.VERSION.SDK_INT in 19..21) {
            try {
                // Create a trust manager that does not validate certificate chains

                val sc = SSLContext.getInstance("TLSv1.2")
                sc.init(null, null, null)
                client.sslSocketFactory(
                    Tls12SocketFactory(sc.socketFactory),
                    Platform.get().trustManager(sc.socketFactory)
                )
                val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .build()

                val specs = ArrayList<ConnectionSpec>()
                specs.add(cs)
                specs.add(ConnectionSpec.COMPATIBLE_TLS)
                specs.add(ConnectionSpec.CLEARTEXT)

                client.connectionSpecs(specs)

            } catch (exc: Exception) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc)
            }
        }

        return client
    }

    fun handleApiError(body: ResponseBody): String {
        val errorConverter: Converter<ResponseBody, Status> =
            mRetrofit.responseBodyConverter(Status::class.java, arrayOfNulls(0))
        val error: Status? = errorConverter.convert(body)
        return error?.error.toString()
    }

}